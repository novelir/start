# Start - Sandbox utility for piles

[**Novelir is still in heavy development.**](https://novelir.org/)


## Dependencies

### Build dependencies

 - qt6-base
 - cmake
 - make

## Installation

    $ mkdir build
    $ cd build
    $ cmake -DCMAKE_INSTALL_PREFIX:PATH=<PREFIX> ..
    $ make
    # make install

Default `<PREFIX>` is `/usr/local`


## Usage

    $ ln -s <PREFIX>/bin/start path/to/program
    $ path/to/program

## Links

- **Get involved at [Novelir.org](https://novelir.org/)**
- **Lean more on the [Wiki](https://gitlab.com/novelir/website/-/wikis)**
