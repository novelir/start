/****************************************************************************
**
** Copyright © 2021 Arthur Autin, Novelir
**
** This file is part of Novelir.
**
** Novelir is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Novelir is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Novelir. If not, see <https://www.gnu.org/licenses/>.
**
****************************************************************************/

#include <QCoreApplication>
#include <QSettings>
#include <QDir>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QProcess>
#include <QRandomGenerator>
#include <QStandardPaths>
#include <QDebug>

#include <unistd.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/syscall.h>

#include "main.hpp"

int main(int argc, char *argv[])
{
    QCoreApplication::setSetuidAllowed(true);

    QCoreApplication a(argc, argv);
    QCoreApplication::setOrganizationName("novelir");
    QCoreApplication::setApplicationName("start");
    QCoreApplication::setApplicationVersion("0.0.2");


// Declare the pile database
    QSettings pileDb(QSettings::SystemScope, QCoreApplication::organizationName(), "pile");


// Get the name of the program to execute
    QStringList programArgs = a.arguments();
    QString program = QFileInfo(programArgs[0]).fileName();
    programArgs.removeFirst();


// Get the name of the pile
    pileDb.beginGroup("Program");
    pileDb.beginGroup(program);

    QString pile = pileDb.value("pile").toString();

    pileDb.endGroup();
    pileDb.endGroup();

    pileDb.beginGroup("Pile");
    if ( pile.isEmpty() || !pileDb.childGroups().contains(pile) ) {
        return 1;
    }
    pileDb.endGroup();


// Get the list of dependencies
    pileDb.beginGroup("Pile");
    pileDb.beginGroup(pile);
    QStringList depList = pileDb.value("dependencyList").toStringList();
    pileDb.endGroup();
    pileDb.endGroup();


// Get the default profile
    QJsonObject jsonProfileRO;
    {
        QFile fileRO("/pile/" + pile + "/manifest.json");

        if ( fileRO.exists() && fileRO.open(QIODevice::ReadOnly | QIODevice::Text) ) {
            QJsonDocument manifestJsonRO = QJsonDocument::fromJson(fileRO.readAll());
            fileRO.close();

            QJsonObject jsonRootRO = manifestJsonRO.object();
            for ( QJsonValue value : jsonRootRO["program"].toArray() ) {
                if ( value["id"].toString() == program ) {
                    jsonProfileRO = value.toObject();
                    break;
                }
            }
        }
        else {
            return 1;
        }
    }


// Get the user profile
    QJsonObject jsonProfileRW;
    {
        QFile fileRW(QDir().homePath() + "/.pile/" + pile + "/profile/" + program + ".json");
        if ( fileRW.exists() && fileRW.open(QIODevice::ReadOnly | QIODevice::Text) ) {
            QJsonDocument manifestJsonRW = QJsonDocument::fromJson(fileRW.readAll());
            fileRW.close();

            jsonProfileRW = manifestJsonRW.object();
        }
    }



// Save the user's UID/GID and switch to root account (the executable must be setuid)
    const unsigned int gid = getgid();
    const unsigned int uid = getuid();
    setgid(geteuid());
    setuid(geteuid());



// Create the per-user pile's directory
    const QString rootfs = "/pile/" + pile;

    QString overlayRootfs = "";
    {
        QDir qdir;
        do {
            qdir.setPath( QStandardPaths::standardLocations(QStandardPaths::TempLocation)[0] + "/start/root/" + pile + '/' +  QString::number(QRandomGenerator::global()->generate64()));
        } while ( qdir.exists() );
        qdir.mkpath(qdir.path());
        overlayRootfs = qdir.path();
    }
    QString overlayMutable = QDir().homePath() + "/.pile/" + pile + "/mutable";
    {
        setegid(gid);
        seteuid(uid);
        for ( QString dir : {"cache", "configuration", "data", "data/Trash"} ) {
            QDir().mkpath(overlayMutable + '/' + dir);
        }
        QDir().mkpath(QDir().homePath() + "/.pile/" + pile + "/profile");
        setegid(getgid());
        seteuid(getuid());
    }

    QString workRootfs = "";
    {
        QDir qdir;
        do {
            qdir.setPath( QStandardPaths::standardLocations(QStandardPaths::TempLocation)[0] + "/start/work/" + pile + '/' +  QString::number(QRandomGenerator::global()->generate64()));
        } while ( qdir.exists() );
        workRootfs = qdir.path() + "/rootfs";
        qdir.mkpath(workRootfs);
    }
    QString workMutable = "";
    {
        QDir qdir;
        do {
            qdir.setPath( QStandardPaths::standardLocations(QStandardPaths::CacheLocation)[0] + "/work/" + pile + '/' +  QString::number(QRandomGenerator::global()->generate64()));
        } while ( qdir.exists() );
        workMutable = qdir.path() + "/mutable";
        qdir.mkpath(workMutable);
    }



// Mount namespace
    if ( unshare(CLONE_NEWNS) ) {
        qCritical() << "mount namespace";
        return 1;
    }
    else if ( mount("none", "/", NULL, MS_SLAVE | MS_REC, NULL) ) {
        qCritical() << "Mount /";
        return 1;
    }



// Mount the new root filesystem
    {
        QStringList lowerdirs = QStringList() << rootfs;
        for ( QString dep : depList ) {
            lowerdirs << "/pile/" + dep;
        }
        if ( mount("/",
                   rootfs.toUtf8(),
                   "overlay",
                   MS_NOATIME,
                   "lowerdir=" + lowerdirs.join(':').toUtf8() + ",upperdir=" + overlayRootfs.toUtf8() + ",workdir=" + workRootfs.toUtf8()) ) {
            qCritical() << "Overlay";
            return 1;
        }
    }



// Populate /home
    {
        QDir().mkpath(rootfs + QDir().homePath());
        QFile(rootfs + QDir().homePath()).setPermissions(QFileInfo(QDir().homePath()).permissions());
        chown(rootfs.toUtf8() + QDir().homePath().toUtf8(), uid, gid);
    }


// Populate /resource/mutable
    QDir().mkpath(rootfs + "/resource/mutable");
    if ( mount("/resource/mutable",
               rootfs.toUtf8() + "/resource/mutable",
               "overlay",
               MS_NOATIME | MS_NODEV | MS_NODEV | MS_NOSUID | MS_NOEXEC,
               "lowerdir=" + rootfs.toUtf8() + "/resource/mutable" + ",upperdir=" + overlayMutable.toUtf8() + ",workdir=" + workMutable.toUtf8()) ) {
        qCritical() << "Mutable";
        return 1;
    }


// Populate /runtime
    {
        QDir().mkdir(rootfs + "/runtime");

        QDir().mkdir(rootfs + "/runtime/host");
            if ( mount("none", rootfs.toUtf8() + "/runtime/host", "tmpfs", MS_NOATIME | MS_NODEV | MS_NOSUID | MS_NOEXEC, NULL) ) {
                qCritical() << "/runtime/host";
                return 1;
            }

        QDir().mkdir(rootfs + "/runtime/host/media");
        QDir().mkdir(rootfs + "/runtime/host/temporary");
            chmod(rootfs.toUtf8() + "/runtime/host/temporary", 01777);
            chown(rootfs.toUtf8() + "/runtime/host/temporary", uid, gid);

        QDir().mkdir(rootfs + "/runtime/kernel");
        QDir().mkdir(rootfs + "/runtime/kernel/boot");
        QDir().mkdir(rootfs + "/runtime/kernel/dev");
        QDir().mkdir(rootfs + "/runtime/kernel/proc");
        QDir().mkdir(rootfs + "/runtime/kernel/sys");

        QDir().mkdir(rootfs + "/runtime/user");
        QDir().mkdir(rootfs + "/runtime/user/" + QString::number(uid));
            if ( mount("none", rootfs.toUtf8() + "/runtime/user/" + QString::number(uid).toUtf8(), "tmpfs", MS_NOATIME | MS_NODEV | MS_NOSUID | MS_NOEXEC, NULL) ) {
                qCritical() << "/runtime/user/" + QString::number(uid);
                return 1;
            }
            chmod(rootfs.toUtf8() + "/runtime/user/" + QString::number(uid).toUtf8(), 00700);
            chown(rootfs.toUtf8() + "/runtime/user/" + QString::number(uid).toUtf8(), uid, gid);
    }



// (F)ILESYSTEM (H)IERARCHY (S)TANDARD (L)AYER :
    {
        QFile().link("resource/program", rootfs + "/bin");
        QFile().link("runtime/kernel/boot", rootfs + "/boot");
        QFile().link("runtime/kernel/dev", rootfs + "/dev");
        QFile().link("resource/mutable/configuration", rootfs + "/etc");
        QFile().link("resource/immutable/headers", rootfs + "/include");
        QFile().link("resource/immutable/libraries", rootfs + "/lib");
        QFile().link("resource/immutable/libraries", rootfs + "/lib64");
        QFile().link("resource/immutable/libraries", rootfs + "/libexec");
        QFile().link(".", rootfs + "/local");
        QFile().link("runtime/host/media", rootfs + "/media");
        QFile().link("runtime/kernel/proc", rootfs + "/proc");
        QFile().link("runtime/host", rootfs + "/run");
        QFile().link("resource/program", rootfs + "/sbin");
        QFile().link("resource/immutable/data", rootfs + "/share");
        QFile().link("resource/mutable/data", rootfs + "/srv");
        QFile().link("runtime/kernel/sys", rootfs + "/sys");
        QFile().link("runtime/host/temporary", rootfs + "/tmp");
        QFile().link(".", rootfs + "/usr");
        QFile().link("resource/mutable/data", rootfs + "/var");
        QFile().link("../../../runtime/host", rootfs + "/var/run");
    }



// Additional filesystem
    {
        QJsonArray filesystem;

    // Add manifest files and directories
        filesystem << jsonProfileRO["filesystem"].toArray();

    // Add profile files and directories
        for ( QJsonValue element : jsonProfileRW["filesystem"].toArray() ) {
            filesystem << element.toObject();
        }

    // Add system-wide files and directories
        {
            QList<QVariantList> bindList;
            {
            // Necessary bind-mounts
                bindList << QVariantList({"/opt", true, false});
                bindList << QVariantList({"/dev/dri", true, false});
                bindList << QVariantList({"/dev/null", true, true});
                bindList << QVariantList({"/etc/resolv.conf", true, false});
                bindList << QVariantList({"/resource/immutable/fonts", true, false});
                bindList << QVariantList({"/resource/immutable/icons", true, false});
                bindList << QVariantList({"/resource/immutable/mime", true, false});
                bindList << QVariantList({"/resource/immutable/themes", true, false});
                if ( ( jsonProfileRW["mount"].isNull() && jsonProfileRO["mount"].toBool() == false ) ||
                     (!jsonProfileRW["mount"].isNull() && jsonProfileRW["mount"].toBool() == false ) ) {
                    bindList << QVariantList({"/runtime/host/media", true, true});
                }

            // Audio.in access
                if ( (jsonProfileRW["device"].toObject().value("audio.in").isUndefined() && jsonProfileRO["device"].toObject().value("audio.in").toBool(false)) ||
                     jsonProfileRW["device"].toObject().value("audio.in").toBool(false) ) {
                    QDir dev("/dev/snd");
                    dev.setFilter(QDir::Files | QDir::System);
                    dev.setNameFilters({"pcmC*D*c", "hwC*D*", "controlC*", "timer"});

                    for ( QString device : dev.entryList() ) {
                        bindList << QVariantList({"/dev/snd/" + device, true, false});
                    }
                }

            // Audio.out access
                if ( (jsonProfileRW["device"].toObject().value("audio.out").isUndefined() && jsonProfileRO["device"].toObject().value("audio.out").toBool(false)) ||
                     jsonProfileRW["device"].toObject().value("audio.out").toBool(false) ) {
                    QDir dev("/dev/snd");
                    dev.setFilter(QDir::Files | QDir::System);
                    dev.setNameFilters({"pcmC*D*p", "controlC*", "timer"});

                    for ( QString device : dev.entryList() ) {
                        bindList << QVariantList({"/dev/snd/" + device, true, false});
                    }
                }

            // Video access
                if ( (jsonProfileRW["device"].toObject().value("video").isUndefined() && jsonProfileRO["device"].toObject().value("video").toBool(false)) ||
                     jsonProfileRW["device"].toObject().value("video").toBool(false) ) {
                    QDir dev("/dev/");
                    dev.setFilter(QDir::Files | QDir::System);
                    dev.setNameFilters({"video*"});

                    for ( QString device : dev.entryList() ) {
                        bindList << QVariantList({"/dev/" + device, true, false});
                    }
                }
            }


            for ( QVariantList pair : bindList ) {
                QMap<QString, QVariant> var;
                var["path"] = pair[0];
                var["read"] = pair[1];
                var["write"] = pair[2];

                filesystem << QJsonValue::fromVariant(var);
            }
        }


    // Create the bind mounts
        for ( QJsonValue fs : filesystem ) {
            QJsonObject fsObj = fs.toObject();
            QString path = fsObj["path"].toString();
            const bool read = fsObj["read"].toBool(true);
            const bool write = fsObj["write"].toBool(false);


        // Replace '~' with the actual home folder
            if ( path.startsWith('~') ) {
                path.replace(0, 1, QDir::homePath());
            }

        // Replace environment variables with their values
        /// Security issue ? May be removed later
            QStringList pathParts = path.split('/');
            for ( int i = 0; i < pathParts.size(); ++i ) {
                if ( pathParts[i].startsWith('$') ) {
                    pathParts.replace(i, qgetenv(qUtf8Printable(pathParts[i].remove(0, 1))));
                }
            }
            path = pathParts.join('/');

        // Check that the path is absolute
            if ( !path.isEmpty() && path[0] != '/' ) {
                continue;
            }

        // Check that the file/directory exists
            bool isDir = false;
            if ( !QFile(path).exists() ) {
                continue;
            }
            else if ( QDir(path).exists() ) {
                isDir = true;
            }


        // Bind the path if read=true
            if ( read ) {
            // Create needed directories
                {
                    QStringList parts = {};
                    for ( QString part : path.split('/')) {
                        parts << part;
                        if ( ( !isDir && !part.isEmpty() && !QFile(overlayRootfs + parts.join('/')).exists() && part != path.split('/').last() ) ||
                             ( isDir && !part.isEmpty() && !QFile(overlayRootfs + parts.join('/')).exists() ) ) {

                            QFileInfo fileInfo = QFileInfo(parts.join('/'));

                            if ( path.startsWith("/mutable") ) {
                                QDir().mkpath(overlayMutable + parts.join('/'));
                                QFile(overlayMutable + parts.join('/')).setPermissions(fileInfo.permissions());
                                chown(overlayMutable.toUtf8() + parts.join('/').toUtf8(), uid, gid);
                            }
                            else {
                                QDir().mkpath(overlayRootfs + parts.join('/'));
                                QFile(overlayRootfs + parts.join('/')).setPermissions(fileInfo.permissions());
                                chown(overlayRootfs.toUtf8() + parts.join('/').toUtf8(), fileInfo.ownerId(), fileInfo.groupId());
                            }
                        }
                    }
                }
            // Create needed empty files
                if ( !isDir ) {
                    QFile(rootfs + path).open(QIODevice::ReadWrite);
                    QFile(rootfs + path).close();
                }

            // Bind-mount
                unsigned long mntOpt = MS_REMOUNT | MS_BIND;
                if ( !write ) {
                    mntOpt = mntOpt | MS_RDONLY;
                }
                if ( !path.startsWith("/runtime/kernel/dev") &&
                     !path.startsWith("/dev")) {
                    mntOpt = mntOpt | MS_NODEV;
                }

                mount(path.toUtf8(), qUtf8Printable(rootfs + path), NULL, MS_BIND | MS_NOSUID, NULL);
                mount(path.toUtf8(), qUtf8Printable(rootfs + path), NULL, mntOpt | MS_NOSUID, NULL);
            }

        // Unbind the path if read=false
            else {
                qDebug() << path << read << umount(qUtf8Printable(rootfs + path));
            }
        }
    }



// Switch to the new root
    if ( chdir(rootfs.toUtf8()) ) {
        qCritical() << "Chdir";
        return 1;
    }
    if ( syscall(SYS_pivot_root, ".", ".") ) {
        qCritical() << "Pivot";
        return 1;
    }
    if ( umount2(".", MNT_DETACH) ) {
        qCritical() << "Umount";
        return 1;
    }
    if ( chroot(".") ) {
        qCritical() << "Chroot";
        return 1;
    }



// Namespaces
    {
    // IPC namespace
        if ( (jsonProfileRW["namespace"].toObject().value("ipc").isUndefined() && !jsonProfileRO["namespace"].toObject().value("ipc").toBool(false)) ||
             !jsonProfileRW["namespace"].toObject().value("ipc").toBool(false) ) {
            if ( unshare(CLONE_NEWIPC) ) {
                qCritical() << "IPC";
                return 1;
            }
        }

    // Cgroup namespace
        if ( (jsonProfileRW["namespace"].toObject().value("cgroup").isUndefined() && !jsonProfileRO["namespace"].toObject().value("cgroup").toBool(false)) ||
             !jsonProfileRW["namespace"].toObject().value("cgroup").toBool(false) ) {
            if ( unshare(CLONE_NEWCGROUP) ) {
                qCritical() << "Cgroup";
                return 1;
            }
        }

    // PID namespace
        if ( (jsonProfileRW["namespace"].toObject().value("pid").isUndefined() && !jsonProfileRO["namespace"].toObject().value("pid").toBool(false)) ||
             !jsonProfileRW["namespace"].toObject().value("pid").toBool(false) ) {
            if ( unshare(CLONE_NEWPID) ) {
                qCritical() << "PID";
                return 1;
            }
        }

    // UTS namespace
        if ( (jsonProfileRW["namespace"].toObject().value("uts").isUndefined() && !jsonProfileRO["namespace"].toObject().value("uts").toBool(false)) ||
             !jsonProfileRW["namespace"].toObject().value("uts").toBool(false) ) {
            if ( unshare(CLONE_NEWUTS) ) {
                qCritical() << "UTS";
                return 1;
            }
        }

    // Network namespace
        if ( (jsonProfileRW["namespace"].toObject().value("network").isUndefined() && !jsonProfileRO["namespace"].toObject().value("network").toBool(false)) ||
             !jsonProfileRW["namespace"].toObject().value("network").toBool(false) ) {
            if ( unshare(CLONE_NEWNET) ) {
                qCritical() << "Network";
                return 1;
            }
        }

    // User namespace
        setgid(gid);
        setuid(uid);
        if ( (jsonProfileRW["namespace"].toObject().value("user").isUndefined() && !jsonProfileRO["namespace"].toObject().value("user").toBool(false)) ||
             !jsonProfileRW["namespace"].toObject().value("user").toBool(false) ) {
            if ( unshare(CLONE_NEWUSER) ) {
                qCritical() << "User";
                return 1;
            }
        }
    }



// Capabilities
    {
/// WIP
    }



// Environment variables
    QProcessEnvironment execEnv = QProcessEnvironment::systemEnvironment();
    {
        QJsonArray environment = jsonProfileRO["environment"].toArray();
        for ( QJsonValue element : jsonProfileRW["environment"].toArray() ) {
            environment << element.toObject();
        }

        for ( QJsonValue env : environment ) {
            QJsonObject envObj = env.toObject();
            if ( !envObj["variable"].toString().isEmpty() && !envObj["value"].toString().isEmpty() ) {
                execEnv.insert(envObj["variable"].toString(), envObj["value"].toString());
            }
        }
    }



// Start the program
    {
        QProcess *exec = new QProcess();
        exec->setProcessChannelMode(QProcess::ForwardedChannels);
        exec->setInputChannelMode(QProcess::ForwardedInputChannel);

        exec->setProcessEnvironment(execEnv);
        exec->start("/resource/program/" + program, programArgs);

        exec->waitForFinished(-1);

        return exec->exitCode();
    }
}
